entangle (3.0-4) unstable; urgency=medium

  * Team upload.
  * debian/control: move to libgdk-pixbuf-2.0-dev b-dep. (Closes: #1037370)

  [ Debian Janitor ]
  * Refer to common license file for CC0-1.0.
  * Update standards version to 4.6.1, no changes needed.

 -- Bastian Germann <bage@debian.org>  Sun, 17 Sep 2023 22:35:09 +0200

entangle (3.0-3) unstable; urgency=medium

  * debian/control: add gir package to binary dependencies
    (Closes: #1001814)

 -- Matteo F. Vescovi <mfv@debian.org>  Thu, 07 Apr 2022 22:44:52 +0200

entangle (3.0-2) unstable; urgency=medium

  * debian/patches/: patchset updated
    - 0001-Fix_meson_build.patch added (Closes: #1005580)
  * debian/copyright:
    - extend copyright timestamp
    - add CC0-1.0 stanza
  * debian/control: S-V bump 4.5.0 -> 4.6.0 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Tue, 15 Mar 2022 23:06:29 +0100

entangle (3.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control:
    - debhelper bump 12 -> 13
    - add 'appstream' as B-D
  * debian/entangle.docs: README -> README.rst
  * debian/: fix appdata/metainfo stuff

 -- Matteo F. Vescovi <mfv@debian.org>  Thu, 06 Aug 2020 10:54:47 +0200

entangle (2.0-2) unstable; urgency=medium

  * debian/: debhelper bump 11 -> 12
  * debian/control:
    - drop libdbus-glib-1-dev b-dep (Closes: #955837)
    - S-V bump 4.3.0 -> 4.5.0 (no changes needed)
    - RRR set
    - http:// -> https:// for Homepage field
  * debian/: fix documentation path install
  * debian/entangle.install: install plugins

 -- Matteo F. Vescovi <mfv@debian.org>  Tue, 07 Apr 2020 01:08:27 +0200

entangle (2.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control: S-V bump 4.2.1 -> 4.3.0 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Mon, 21 Jan 2019 23:01:12 +0100

entangle (1.0-2) unstable; urgency=medium

  * debian/rules: add '--no-parallel' option to dh (Closes: #909836)

 -- Matteo F. Vescovi <mfv@debian.org>  Wed, 28 Nov 2018 23:38:59 +0100

entangle (1.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #908495, #889172
  * d/watch: Enable more flexible extensions to fetch latest upstream
  * Point Vcs fields to Salsa
  * New upstream version
  * debhelper 11
  * Standards-Version: 4.2.1
  * Drop manual -dbg package
  * Upstream switched from automake to meson
  * Move appdata from legacy location to new location
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Mon, 24 Sep 2018 22:21:30 +0200

entangle (0.7.2-1) unstable; urgency=medium

  * New upstream release
    - debian/patches/: patchset dropped (fixed upstream)
    - debian/control: gstreamer b-deps added
    - debian/entangle.doc-base: sgml documentation dropped

 -- Matteo F. Vescovi <mfv@debian.org>  Tue, 29 Aug 2017 22:53:00 +0200

entangle (0.7.1-3) unstable; urgency=medium

  * Upload to unstable
  * debian/control: S-V bump to 4.0.0 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Thu, 22 Jun 2017 17:25:20 +0200

entangle (0.7.1-2) experimental; urgency=medium

  * debian/patches/: patchset updated
    - 0001-Drop_gnome-icon-theme-symbolic_check added (Closes: #859267)
  * debian/control: drop obsolete b-dep and add ad-hoc Recommends at runtime
  * debian/control: S-V bump 3.9.7 -> 3.9.9 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Sat, 13 May 2017 15:27:18 +0200

entangle (0.7.1-1) unstable; urgency=medium

  * New upstream release
    - debian/patches/: patchset dropped (applied upstream)
  * debian/control: S-V bump 3.9.6 -> 3.9.7 (no changes needed)
  * debian/control: Vcs-* fields updated for https:// usage

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 18 Mar 2016 09:15:21 +0100

entangle (0.7.0-3) unstable; urgency=medium

  * debian/entangle.install: missing directory added.
    Thanks to Pascal Mons for pointing this out.

 -- Matteo F. Vescovi <mfv@debian.org>  Wed, 13 May 2015 08:45:22 +0200

entangle (0.7.0-2) unstable; urgency=medium

  * Upload to unstable

 -- Matteo F. Vescovi <mfv@debian.org>  Tue, 28 Apr 2015 15:21:11 +0200

entangle (0.7.0-1) experimental; urgency=medium

  * New upstream release
    - debian/control: itstool b-dep added
    - debian/control: gtk-doc-tools b-dep added
    - debian/control: yelp-tools b-dep added
    - debian/entangle.install: shared libraries install
    - debian/patches/: new patchset setup
      - 0001-Fix_linker_issue.patch added

 -- Matteo F. Vescovi <mfv@debian.org>  Wed, 18 Mar 2015 17:30:18 +0100

entangle (0.6.1-2) experimental; urgency=medium

  * debian/entangle.install: missing directories added.
    Thanks to Pascal Mons for pointing it out.

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 27 Feb 2015 08:35:04 +0100

entangle (0.6.1-1) experimental; urgency=medium

  * New upstream release
    - debian/copyright: copyrights and licenses updated
  * debian/upstream/: signing key added
    - debian/watch: GnuPG signature verification added
  * debian/control: S-V bump 3.9.5 => 3.9.6 (no changes needed)
  * debian/control: Uploader e-mail address updated

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 06 Feb 2015 09:36:41 +0100

entangle (0.6.0-2) unstable; urgency=medium

  * debian/: autotools => dh-autoreconf

 -- Matteo F. Vescovi <mfvescovi@gmail.com>  Thu, 31 Jul 2014 15:25:09 +0200

entangle (0.6.0-1) unstable; urgency=medium

  [ Jackson Doak ]
  * Configure with --disable-silent-rules
  * debian/rules: --disable-silent-rules parameter added

  [ Matteo F. Vescovi ]
  * New upstream release
    - debian/control: b-dep liblcms1-dev => liblcms2-dev (Closes: #745527)
    - debian/control: gnome-icon-theme-symbolic b-dep added

 -- Matteo F. Vescovi <mfvescovi@gmail.com>  Sat, 03 May 2014 10:17:34 +0200

entangle (0.5.4-2) unstable; urgency=medium

  * debian/control: b-dep libgphoto2-2-dev => libgphoto2-dev (Closes: #739354)
  * debian/control: S-V bump 3.9.4 => 3.9.5 (no changes needed)

 -- Matteo F. Vescovi <mfvescovi@gmail.com>  Mon, 24 Feb 2014 09:15:05 +0100

entangle (0.5.4-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Uploader e-mail address updated

 -- Matteo F. Vescovi <mfvescovi@gmail.com>  Mon, 16 Dec 2013 17:21:38 +0100

entangle (0.5.3-1) unstable; urgency=low

  * New upstream release
    - debian/rules: gschemas.compiled file removal dropped.
      This line was added to fix #689576, now applied upstream.

 -- Matteo F. Vescovi <mfv.debian@gmail.com>  Fri, 30 Aug 2013 18:43:43 +0200

entangle (0.5.2-1) unstable; urgency=low

  * New upstream release

 -- Matteo F. Vescovi <mfv.debian@gmail.com>  Mon, 19 Aug 2013 14:46:31 +0200

entangle (0.5.1-1) unstable; urgency=low

  * New upstream release
  * debian/rules: "gir" addon added to dh parameters

 -- Matteo F. Vescovi <mfv.debian@gmail.com>  Wed, 13 Mar 2013 09:04:41 +0100

entangle (0.5.0-1) unstable; urgency=low

  [ Matteo F. Vescovi ]
  * New upstream release
  * debian/control: libraw-dev added to b-deps
  * debian/entangle.install: install paths fixed
  * debian/rules: useless dirs and files removed
  * debian/: gir1.2-entangle-0.1 package added (Closes: #695342)

  [ Matthieu Baerts ]
  * debian/control:
    - Bumped Standards-Version (no changes needed)
  * debian/rules:
    - Removed usr/share/glib-2.0/schemas/gschemas.compiled (Closes: #689576)

 -- Matteo F. Vescovi <mfv.debian@gmail.com>  Thu, 10 Jan 2013 09:23:59 +0100

entangle (0.4.1-1) unstable; urgency=low

  * New upstream release
    - debian/: ad-hoc manpages removed

 -- Matteo F. Vescovi <mfv.debian@gmail.com>  Fri, 07 Sep 2012 14:51:51 +0200

entangle (0.4.0-1) unstable; urgency=low

  * Initial release (Closes: #645972)

 -- Matteo F. Vescovi <mfv.debian@gmail.com>  Fri, 10 Aug 2012 17:29:21 +0200
