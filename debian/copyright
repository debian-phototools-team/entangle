Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: entangle
Upstream-Contact: Daniel P. Berrangé <dan@berrange.com>
Source: http://entangle-photo.org/

Files: *
Copyright: 2009-2022 Daniel P. Berrangé <dan@berrange.com>
License: GPL-3+

Files: debian/*
Copyright: 2012-2018 Andreas Tille <tille@debian.org>
           2012-2022 Matteo F. Vescovi <mfv@debian.org>
License: GPL-3+

Files: src/org.entangle_photo.Manager.metainfo.xml
Copyright: 2009-2022 Daniel P. Berrangé <dan@berrange.com>
License: CC0-1.0

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the full text of the CC0 1.0 Universal license can be found
 in the file `/usr/share/common-licenses/CC0-1.0'.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".
